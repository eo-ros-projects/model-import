#ifndef IMPORTURDF_HPP
#define IMPORTURDF_HPP

#include <cmath>
#include <iostream>
#include <string>

//#include <kdl/frames.hpp>
//#include <kdl/joint.hpp>
#include <kdl/tree.hpp>
//#include <kdl/rotationalinertia.hpp>
#include <kdl_parser/kdl_parser.hpp>
#include <ros/param.h>
#include <urdf/model.h>

//DEBUG
// #include <typeinfo>

namespace ImportModel
{

std::string PrintKDLVector(const KDL::Vector& vec);

std::string PrintMat3x3FromArray(const double data[9]);

//Note: translation only, not a full transformation between frames/origins (rotation + PA)
void CalcParallelAxisInertia(const double mass, const KDL::Vector& displ, double par_axis_inertia[9]);

void AddParallelAxisInertia(const double inertia_in[9], const double mass, const KDL::Vector& displ, double inertia_out[9]);

void SubtractParallelAxisInertia(const double inertia_in[9], const double mass, const KDL::Vector& displ, double inertia_out[9]);

void ListModelAttributes(const KDL::Tree& tree);

bool ImportParamToKDL(std::string robot_desc, KDL::Tree& kdl_tree);

} // namespace ImportModel

#endif // IMPORTURDF_HPP