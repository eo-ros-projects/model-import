
#include <iostream>

#include <kdl/tree.hpp>
#include <ros/ros.h>

#include "ImportURDF.hpp"

int main(int argc, char *argv[])
{
    //TODO: make sure /robot_description exists, else error + terminate

    std::string lump_robot_descrip;
    std::string robot_desc_name("/robot_description");
    KDL::Tree kdl_tree;

    //Note: giving this a node name allows for error-less URDF import, may be best to just make things nodes
    //unless they're pure libraries
    ros::init(argc, argv, "my_urdf_import_node");

    //TODO: just holding the file for now, need to create similar launch file
    //urdf::Model::initFile("/home/e/ros/catkin_ws/src/cute_robot/cute_model/robots/cyte_300es.urdf.xacro");

    if(ImportModel::ImportParamToKDL(robot_desc_name, kdl_tree))
    {
        ImportModel::ListModelAttributes(kdl_tree);
    }

    std::cout << "Robot imported as KDL model and parsed successfully" << std::endl;    
}