
#include "ImportURDF.hpp"

namespace ImportModel
{
//TODO: move these to separate file/library
std::string PrintKDLVector(const KDL::Vector& vec)
{
    return "[" + std::to_string(vec(0)) + ", " + std::to_string(vec(1)) + ", " + std::to_string(vec(2)) + "]";
}

//Doesn't take object directly, accesses internal data to reduce duplication between Inertia and Rotation types
std::string PrintMat3x3FromArray(const double data[9])
{
    //TODO: missing formatting (left padding, spacing)
    return std::to_string(data[0]) + ", " + std::to_string(data[1]) + ", " + std::to_string(data[2]) + "\n"
        + std::to_string(data[3]) + ", " + std::to_string(data[4]) + ", " + std::to_string(data[5]) + "\n"
        + std::to_string(data[6]) + ", " + std::to_string(data[7]) + ", " + std::to_string(data[8]);
}

//TODO: need to move these to my_kd_library
//TODO: pass-by-reference for arrays?
//Note: translation only, not a full transformation between frames/origins (rotation + PA)
void CalcParallelAxisInertia(const double mass, const KDL::Vector& displ, double par_axis_inertia[9])
{
    //Source: https://hepweb.ucsd.edu/ph110b/110b_notes/node25.html (Note: other methods led to incorrect off-diagonals)

    par_axis_inertia[0] = mass*(pow(displ[1], 2) + pow(displ[2], 2));
    par_axis_inertia[4] = mass*(pow(displ[0], 2) + pow(displ[2], 2));
    par_axis_inertia[8] = mass*(pow(displ[0], 2) + pow(displ[1], 2));
    par_axis_inertia[1] = par_axis_inertia[3] = -mass*displ[0]*displ[1];
    par_axis_inertia[2] = par_axis_inertia[6] = -mass*displ[0]*displ[2];
    par_axis_inertia[5] = par_axis_inertia[7] = -mass*displ[1]*displ[2];
}

void AddParallelAxisInertia(const double inertia_in[9], const double mass, const KDL::Vector& displ, double inertia_out[9])
{
    double par_axis_inertia[9];
    CalcParallelAxisInertia(mass, displ, par_axis_inertia);
    for(size_t i = 0; i < 9; ++i)   //TODO: hardcoded limit
        inertia_out[i] = inertia_in[i] + par_axis_inertia[i];
}

//TODO: almost entirely duplicates from AddParallelAxisInertia...
void SubtractParallelAxisInertia(const double inertia_in[9], const double mass, const KDL::Vector& displ, double inertia_out[9])
{
    double par_axis_inertia[9];
    CalcParallelAxisInertia(mass, displ, par_axis_inertia);
    for(size_t i = 0; i < 9; ++i)   //TODO: hardcoded limit
        inertia_out[i] = inertia_in[i] - par_axis_inertia[i];
}

void ListModelAttributes(const KDL::Tree& tree)
{
    //std::cout << "KDL Tree Name: " << model.name_ << "\n";
    std::cout << "Number of Tree Segments, Joints: " << tree.getNrOfSegments() << ", " << tree.getNrOfJoints() << "\n";
    std::cout << "KDL Tree Segment Info: \n";
    KDL::SegmentMap tree_segments = tree.getSegments();      // gives const reference, no-copy?
    //KDL::Tree root not has uninitialized parent iterator, causes segfault with attempted access; 
    //use name check as work-around... (map<> requires unique keys)
    std::string tree_root_name = tree.getRootSegment()->first;
    for(auto iter : tree_segments)
    {
        //iter type: pair<string, TreeElement>
        //Parent/children macros give single/vector SegmentMap iterators
        KDL::SegmentMap::const_iterator curr_parent = GetTreeElementParent(iter.second);
        std::vector<KDL::SegmentMap::const_iterator> curr_children = GetTreeElementChildren(iter.second);
        KDL::Segment curr_segment = GetTreeElementSegment(iter.second);

        std::cout << iter.first << "; Parent: ";
        if(iter.first == tree_root_name)
            //Iterator currently points to root segment, calling parent will cause segfault
            std::cout << "(null: root)";
        else
            std::cout << curr_parent->first;
        std::cout << "; Children: ";

        size_t i = 0;
        size_t num_children = curr_children.size();
        if(num_children == 0)
            std::cout << "(null: tip)";
        else
            for(auto child_iter : curr_children)
            {
                ++i;
                
                std::cout << child_iter->first;
                if(i < num_children)
                    std::cout << ", ";
            }

        KDL::Joint curr_joint = curr_segment.getJoint();
        std::cout << ";\n\tJoint: " << curr_joint.getTypeName() << "; Axis: " << PrintKDLVector(curr_joint.JointAxis())
                << "; Origin: " << PrintKDLVector(curr_joint.JointOrigin());

        //Model's inertia is at link origin, not COG/COM - revert parallel axis component as needed
        KDL::RigidBodyInertia curr_inertia = curr_segment.getInertia();
        double curr_COG_inertia[9];
        SubtractParallelAxisInertia(curr_inertia.getRotationalInertia().data, curr_inertia.getMass(), curr_inertia.getCOG(), 
                            curr_COG_inertia);
        std::cout << "\n\tMass: " << curr_inertia.getMass() << "; CoG: " << PrintKDLVector(curr_inertia.getCOG())
                << "; Inertia at Origin: " << PrintMat3x3FromArray(curr_inertia.getRotationalInertia().data)
                << "; Inertia at CoG: " << PrintMat3x3FromArray(curr_COG_inertia);

        //Note: Local frame origin is often, but not always, the same as joint origin (see cute_robot/end_link)
        //Local Frame Rotation: uses joint between self and parent frame, gives ZYX (ypr) order to put info in parent frame
        KDL::Frame curr_frame = curr_segment.getFrameToTip();
        std::cout << "\n\tLocal Frame Origin: " << PrintKDLVector(curr_frame.p) << "; Local Frame Rotation: " 
                << PrintMat3x3FromArray(curr_frame.M.data);

        std::cout << ";" << std::endl;
    }

    std::cout << std::endl;
}

bool ImportParamToKDL(std::string robot_desc, KDL::Tree& kdl_tree)
{
    urdf::Model temp_model;
    if(temp_model.initParam(robot_desc))
    {
        ROS_INFO("Robot model created from URDF via parameter server");
        //ListModelAttributes(robot_urdf_model);
        
        //Note: can't find description/API of urdf::Model, so use KDL as intermediary? (Using code from Willow
        //Garage, pr2_mechanism_model, chain.cpp)

        if (!kdl_parser::treeFromUrdfModel(temp_model, kdl_tree))
        {
            ROS_ERROR("Could not convert URDF model into KDL tree");
            return false;
        }
        else
        {
            ROS_INFO("KDL tree created successfully from URDF");
            return true;
        }
    }
    else
    {
        ROS_ERROR("Parameter '/robot_description' not found or other URDF parsing error");
        return false;
    }
}

} // namespace ImportModel
